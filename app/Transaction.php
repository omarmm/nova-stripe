<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //

    public function payment()
    {
        return $this->belongsTo('App\Payment');
    }

    //Casts of the model dates
protected $casts = [
    'payment_date'=>'datetime',
];
}
