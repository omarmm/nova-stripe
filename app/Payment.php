<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //relations

    

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    //Casts of the model dates
protected $casts = [
    'due_date' => 'date',
    'payment_date'=>'datetime'
];
}
