<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_method_id')->nullable();
            $table->integer('payment_id');
            $table->string('transaction_reference')->nullable();
            $table->string('type')->nullable(); // (authorize', 'purchase', 'refund', 'void')
            $table->string('status')->nullable(); // ('successful', 'failed', 'pending)
            $table->decimal('amount',11);
            $table->string('currency')->nullable();
            $table->string('Paypal_TXN_ID')->nullable();  //(in case of using paypal)
            $table->string('bank_transfer_certificate')->nullable();  //(in case of using bank account)
            $table->tinyInteger('is_paid')->default(0);
            $table->tinyInteger('seen')->default(0);
            $table->timestamps();
            $table->tinyInteger('created_by')->nullable();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
