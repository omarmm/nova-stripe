<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('entity_id'); //payment type
            $table->integer('item_id');
            $table->integer('user_id');
            $table->decimal('amount',11);
            $table->string('currency')->nullable();
            $table->timestamp('due_date')->nullable();
            $table->timestamp('payment_date')->nullable();
           // $table->string('period')->default('monthly');
            $table->tinyInteger('is_paid')->default(0);
            $table->integer('payment_method_id')->nullable();
            $table->timestamps();
            $table->tinyInteger('created_by')->nullable();
            $table->tinyInteger('updated_by')->nullable();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment');
    }
}
